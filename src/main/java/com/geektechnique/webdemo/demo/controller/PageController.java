package com.geektechnique.webdemo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController //@Controller
public class PageController {


    @RequestMapping("/")
    public String home(){
        return "Test String to return to /";
    }

}
